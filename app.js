// main app fo Ductrail App

var http                  = require("http")
var express               = require('express')
var fs                    = require('fs')
var path                  = require('path')
var moment                = require('moment')
var bodyparser            = require('body-parser')
var pdfMake = require('pdfmake/build/pdfmake.js');
var pdfFonts = require('pdfmake/build/vfs_fonts.js');

pdfMake.vfs = pdfFonts.pdfMake.vfs;

var app = express()

moment.locale("nl")

app.use(bodyparser.urlencoded({extended: true}))
app.use(bodyparser.json())
app.use(express.static("public"))


app.get('/', function(req, res){
  res.render('index.pug')
})

app.get('/tour', function(req, res){
  var ducks = function readDucks() {
    var pad = path.join(__dirname, '/public/data')
    var ducks = fs.readFileSync( pad + '/eendenlijst.txt').toString().split('\r\n')
    duckList = ducks.sort()
    var duckArr = []
    for(var i = 0; i<duckList.length; i++){
      var obj = { 'name' : duckList[i] }
      duckArr.push(obj)
    }
    return duckArr
  }
  res.render('tour.pug', {ducks: ducks(), moment:moment})

})

app.get('/about', function(req, res){
  res.render('about.pug')
})

app.post('/antwform', function (req, res){
// generate antw formulieren
  // var docDefinition = antwForm(req.body)
  console.log(req.body)
  // if(req.body.AnswerForms)

  res.render('antwform.pug')
})


app.listen(8000)
console.log('Het gebeurd allemaal op port 8000')
