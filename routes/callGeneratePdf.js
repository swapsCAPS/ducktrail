const docDefinition = {
  content: ['This will show up in the file created']
};

generatePdf(docDefinition, (response) => {
  res.setHeader('Content-Type', 'application/pdf');
  res.send(response); // Buffer data
});
