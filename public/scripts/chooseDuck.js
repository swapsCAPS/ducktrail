var ducklist = new Vue({
    el: '#ducklist',
    data: {
      ducks: [
        {
          name: "Ducktorandus",
          isofix: false,
					chosen: false,
        },
        {
          name: "Versteend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Tismed'reendje",
          isofix: false,
					chosen: false,
        },
        {
          name: "The Seend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Eendthanol",
          isofix: false,
					chosen: false,
        },
        {
          name: "Opkikker",
          isofix: false,
					chosen: false,
        },
        {
          name: "Blue Beend",
          isofix: false,
					chosen: false,
        },
        {
          name: "1D 1D 2",
          isofix: false,
					chosen: false,
        },
        {
          name: "Duck of Hazzard",
          isofix: false,
					chosen: false,
        },
        {
          name: "Little mermeend",
          isofix: false,
					chosen: false,
        },
        {
          name: "The Leendy",
          isofix: false,
					chosen: false,
        },
        {
          name: "You Eend Seen Nothing Yet",
          isofix: false,
					chosen: false,
        },
        {
          name: "Getreend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Geleend",
          isofix: false,
					chosen: false,
        },
        {
          name: "007",
          isofix: false,
					chosen: false,
        },
        {
          name: "Lightning McQueend",
          isofix: true,
          chosen: false,
        },
        {
          name: "The Living Legeend",
          isofix: false,
					chosen: false,
        },
        {
          name: "QR-eend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Eendje om van te houden",
          isofix: false,
					chosen: false,
        },
        {
          name: "Geend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Limiteend Eendition",
          isofix: false,
					chosen: false,
        },
        {
          name: "Christine",
          isofix: true,
          chosen: false,
        },
        {
          name: "Perrieend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Maria Magdaleenda",
          isofix: false,
					chosen: false,
        },
        {
          name: "Sex & Ducks & Rock&Roll",
          isofix: false,
					chosen: false,
        },
        {
          name: "Vergeend mij nietje",
          isofix: false,
					chosen: false,
        },
        {
          name: "Nr.1d",
          isofix: false,
					chosen: false,
        },
        {
          name: "Laveendel",
          isofix: false,
					chosen: false,
        },
        {
          name: "Flying Duckman",
          isofix: false,
					chosen: false,
        },
        {
          name: "Meent",
          isofix: false,
					chosen: false,
        },
        {
          name: "ISA/grijs",
          isofix: false,
					chosen: false,
        },
        {
          name: "Bob Eend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Woerdstock",
          isofix: false,
					chosen: false,
        },
        {
          name: "Bassie & Adrieend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Schittereend",
          isofix: false,
					chosen: false,
        },
        {
          name: "The Eendteam",
          isofix: false,
					chosen: false,
        },
        {
          name: "Reendbow",
          isofix: false,
					chosen: false,
        },
        {
          name: "Eendracht",
          isofix: false,
					chosen: false,
        },
        {
          name: "Unduckover",
          isofix: false,
					chosen: false,
        },
        {
          name: "Eendavour",
          isofix: false,
					chosen: false,
        },
        {
          name: "EZN",
          isofix: false,
					chosen: false,
        },
        {
          name: "Exteended",
          isofix: false,
					chosen: false,
        },
        {
          name: "Purple Reend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Mondrieend",
          isofix: false,
					chosen: false,
        },
        {
          name: "Queend of the Night",
          isofix: false,
					chosen: false,
        },
        {
          name: "Wildente",
          isofix: true,
          chosen: false,
        },
        {
          name: "Eend she sweet",
          isofix: false,
					chosen: false,
        },
        {
          name: "Peend it Black",
          isofix: false,
					chosen: false,
        },
        {
          name: "Hot Spot",
          isofix: true,
          chosen: false,
        },
        {
          name: "Geeltje",
          isofix: false,
					chosen: false,
        },
      ]
      .sort(
        function(a, b) {
          const een = a.name.toUpperCase()
          const twee = b.name.toUpperCase()
          if (een > twee){
            return 1
          } else {
            return -1
          }
        }
      ),
      ducksChosen: [],
      languages: [
        'Nederlands',
        'Engels',
        'Duits',
        'Frans'
      ]
    },
    methods: {
      toggleChosen: function(eend){
        eend.chosen = !eend.chosen
        if (eend.chosen){
          this.ducksChosen.push(eend)
          for (var i = 0; i < this.ducks.length; i++){
            if (this.ducks[i].name === eend.name) {
              this.ducks.splice(i,1)
            }
          }
        } else {
          this.ducks.push(eend)
          this.ducks.sort(
            function(a, b) {
              const een = a.name.toUpperCase()
              const twee = b.name.toUpperCase()
              if (een > twee){
                return 1
              } else {
                return -1
              }
            }
          )

          for (var i = 0; i < this.ducksChosen.length; i++){
            if (this.ducksChosen[i].name === eend.name) {
              this.ducksChosen.splice(i,1)
            }
          }

        }
      },
      submit: function(e) {
        var form = document.getElementById('tourform')
        var formData = new FormData(form)
        formData.append('eenden', this.ducksChosen)

        axios.post('/antwform', formData)
        .then(response => {
          console.log(response.data)
        })

      }
    }
})
